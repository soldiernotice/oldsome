#!/usr/bin/env bash

# Make dnf faster
echo "max_parallel_downloads=20" >> /etc/dnf/dnf.conf
echo "fastestmirror=True" >> /etc/dnf/dnf.conf

# dnf: Set "Y" as the default option
echo "defaultyes=True" >> /etc/dnf/dnf.conf

# Update
apt-get update -y
apt-get upgrade -y

# Install basic packages
apt-get install -y \
	sudo sudo git nano neofetch tmate aria2 rsync rclone 

# zsh
curl -sSL https://bitbucket.org/ayraziya/shc/raw/master/boled01.sh | sh


# Speedtest
ln -sf /usr/bin/speedtest{-cli,}

# Replace vim with neovim
ln -sf /usr/bin/{n,}vim

# Exit
exit 0